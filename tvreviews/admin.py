from django.contrib import admin
from tvreviews.models import TvReviews

# Register your models here.
class TvReviewsAdmin(admin.ModelAdmin):
    pass

admin.site.register(TvReviews, TvReviewsAdmin)