from django import forms

from tvreviews.models import TvReviews

class TvReviewsForm(forms.ModelForm):
    class Meta:
        model = TvReviews
        fields = [
            "cover_image",
            "review_title",
            "author",
            "rating",
            "genre",
        ]