from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from tvreviews.views import TvReviewsListView


urlpatterns = [
    path("", TvReviewsListView.as_view(), name="tvreviews_list"),
]