from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from tvreviews.models import TvReviews

# Create your views here.
class TvReviewsListView(ListView):
    model = TvReviews
    template_name = "tvreviews/list.html"
    Paginate = 3 