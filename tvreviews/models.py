from django.db import models
from django.core.validators import(
    MaxValueValidator, 
    MinValueValidator
)


# Create your models here.
class TvReviews(models.Model):
    cover_image = models.URLField(null=True)
    review_title = models.CharField(max_length=100)
    author = models.CharField(max_length=40, null=False)
    review = models.TextField(max_length=800, null=False)
    genre = models.CharField(max_length=20, null=False)
    rating = models.SmallIntegerField(
        validators =[
            MinValueValidator(1),
            MaxValueValidator(5),
        ]
    )
